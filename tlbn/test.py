from lang import *

# impl = \(p, q).(not p or q)
create_impl = lambda: \
  LambdaExpr(
    [VarDecl("p", boolType), VarDecl("q", boolType)],
    OrExpr(NotExpr("p"), "q"))

table = [
  resolve(CallExpr(create_impl(), [True, True])),
  resolve(CallExpr(create_impl(), [True, False])),
  resolve(CallExpr(create_impl(), [False, True])),
  resolve(CallExpr(create_impl(), [False, False]))
]

for e in table:
  for var in e.fn.vars:
    if type(var.type) is BoolType:
      assert var.type == boolType

# for e in table:
#   print(e)
#   print(evaluate(e))
#   # reduce(e)
