# spring-19-pl

Programming languages

Each subdirectory contains a language implementation. Each language is
defined by the sets of expressions it provides, denoted by various
abbreviations, which are as follows:

- u -- the language is untyped
- t -- the language is typed
- l -- the language supports lambda expressions
- b -- the language supports boolean operations
- n -- the language supports numeric operations

For example, the language `tlbn` is the simply typed lambda calculus extended
with boolean and numeric operations.

Note that lambda expressions generally include multi-argument lambdas and
(more traditional) function calls, but may also include basic single-argument
lambda abstractions. We generally assume multi-argument lambdas are closed.

In general the `b` language is required for the `n` language.

## Language order

We discussed languages in the following order:

1. ub -- A simple boolean calculator language
2. ul -- The pure untyped lambda calculus with facilities for church encodings.
3. ulb -- This implementation includes big step semantics for lambdas.
4. tbn -- A typed calculator language.
5. tlbn -- A typed calculator language with lambdas.
